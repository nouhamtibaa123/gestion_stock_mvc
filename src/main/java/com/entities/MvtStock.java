package com.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class MvtStock implements Serializable {
	public static final int ENTREE =1;
	public static final int SORTIE=1;
	
	
	 @Id
	 @GeneratedValue
	private long idMvtStock;
	 @Temporal(TemporalType.TIMESTAMP)
	 private Date detaMvt;
	 
	 private BigDecimal quantit�;
	 
	 private int typeMvt;
		 
	 @ManyToOne
	 @JoinColumn(name="idArticle")	 
	private Article article ;

	public long getIdMvtStock() {
		return idMvtStock;
	}

	public void setIdMvtStock(long idMvtStock) {
		this.idMvtStock = idMvtStock;
	}

	public Date getDetaMvt() {
		return detaMvt;
	}

	public void setDetaMvt(Date detaMvt) {
		this.detaMvt = detaMvt;
	}

	public BigDecimal getQuantit�() {
		return quantit�;
	}

	public void setQuantit�(BigDecimal quantit�) {
		this.quantit� = quantit�;
	}

	public int getTypeMvt() {
		return typeMvt;
	}

	public void setTypeMvt(int typeMvt) {
		this.typeMvt = typeMvt;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}
	
	
	








}
