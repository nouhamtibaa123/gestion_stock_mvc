package com.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class CommandeFournisseur implements Serializable {
	 @Id
	 @GeneratedValue
	private long idCommandeFournisseur;
	 
	 
	 @ManyToOne
	    @JoinColumn(name="idFournisseur")
		private Fournisseur fournisseur;
		
		@OneToMany(mappedBy="commandeFournisseur")
		private List<LigneCommandeFournisseur> LigneCommandeFournisseur;
		@Temporal(TemporalType.TIMESTAMP)
		private Date DateCommande;
		public long getIdCommandeFournisseur() {
			return idCommandeFournisseur;
		}
		public void setIdCommandeFournisseur(long idCommandeFournisseur) {
			this.idCommandeFournisseur = idCommandeFournisseur;
		}
		public Fournisseur getFournisseur() {
			return fournisseur;
		}
		public void setFournisseur(Fournisseur fournisseur) {
			this.fournisseur = fournisseur;
		}
		public List<LigneCommandeFournisseur> getLigneCommandeFournisseur() {
			return LigneCommandeFournisseur;
		}
		public void setLigneCommandeFournisseur(List<LigneCommandeFournisseur> ligneCommandeFournisseur) {
			LigneCommandeFournisseur = ligneCommandeFournisseur;
		}
		public Date getDateCommande() {
			return DateCommande;
		}
		public void setDateCommande(Date dateCommande) {
			DateCommande = dateCommande;
		}
		
}
