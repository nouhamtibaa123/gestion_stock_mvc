package com.entities;

import java.io.Serializable;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
@Entity
public class Client implements Serializable {
	@Id
	@GeneratedValue
	private long idClient;
	private String nom;
	private String prenom;
	
	private String adresse;
	
	private String photo;
	
	private String email;
	
	@OneToMany(mappedBy="client")
    private List<CommandeClient> CommandeClient;
	public Client() {
		super();
		// TODO Auto-generated constructor stub
	}

	public long getIdClient() {
		return idClient;
	}

	public void setIdClient(long idClient) {
		this.idClient = idClient;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getPhoto() {
		return photo;
	}

	public List<CommandeClient> getCommandeClient() {
		return CommandeClient;
	}

	public void setCommandeClient(List<CommandeClient> commandeClient) {
		CommandeClient = commandeClient;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	

}
