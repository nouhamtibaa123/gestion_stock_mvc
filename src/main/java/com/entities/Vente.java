package com.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
@Entity
public class Vente implements Serializable {

	@Id
	 @GeneratedValue
	private long idVente;
	
	private String code;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date DateVente;
	
	@OneToMany(mappedBy="vente")
	private List<LigneVente> LigneVente;
	
	
	public long getIdVente() {
		return idVente;
	}
	public void setIdVente(long idVente) {
		this.idVente = idVente;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public Date getDateVente() {
		return DateVente;
	}
	public void setDateVente(Date dateVente) {
		DateVente = dateVente;
	}
	
}