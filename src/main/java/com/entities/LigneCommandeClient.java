package com.entities;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class LigneCommandeClient implements Serializable{
	
	 @Id
	 @GeneratedValue
	private long idLigneCommandeClient;
	 @ManyToOne
	    @JoinColumn(name="idArticle")	 
private Article  article;
	
	 @ManyToOne
	 @JoinColumn(name="idCommandeClient")
private CommandeClient commandeClient;
	public Article getArticle() {
	return article;
}

public void setArticle(Article article) {
	this.article = article;
}

public CommandeClient getCommandeClient() {
	return commandeClient;
}

public void setCommandeClient(CommandeClient commandeClient) {
	this.commandeClient = commandeClient;
}

	public long getIdLigneCommandeClient() {
		return idLigneCommandeClient;
	}

	public void setIdLigneCommandeClient(long idLigneCommandeClient) {
		this.idLigneCommandeClient = idLigneCommandeClient;
	}

	public LigneCommandeClient() {
		super();
		// TODO Auto-generated constructor stub
	}
	 
	 
	
	

}
