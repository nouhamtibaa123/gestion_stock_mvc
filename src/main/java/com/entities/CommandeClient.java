package com.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
@Entity
public class CommandeClient implements Serializable {
	 @Id
	 @GeneratedValue
	private long idCommandeClient;
	
	private String code;
	
	@ManyToOne
    @JoinColumn(name="idClient")
	private Client client;
	
	@OneToMany(mappedBy="commandeClient")
	private List<LigneCommandeClient> LigneCommandeClient;
	@Temporal(TemporalType.TIMESTAMP)
	private Date DateCommande;
	public long getIdCommandeClient() {
		return idCommandeClient;
	}
	public void setIdCommandeClient(long idCommandeClient) {
		this.idCommandeClient = idCommandeClient;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}
	public List<LigneCommandeClient> getLigneCommandeClient() {
		return LigneCommandeClient;
	}
	public void setLigneCommandeClient(List<LigneCommandeClient> ligneCommandeClient) {
		LigneCommandeClient = ligneCommandeClient;
	}
	public Date getDateCommande() {
		return DateCommande;
	}
	public void setDateCommande(Date dateCommande) {
		DateCommande = dateCommande;
	}
	
}
