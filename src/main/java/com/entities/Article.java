package com.entities;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="article")
public class Article implements Serializable {
    @Id
    @GeneratedValue
	private long idArticle;

    String codeArticles;
    String designation;
    BigDecimal prixUnitaireHT;
    
    BigDecimal tauxTva;
    
    BigDecimal prixUnitaireTTc;
    
    String photo;
    
    @ManyToOne
    @JoinColumn(name="idCategorie")
    Categorie categorie;
    
    
    
    public Article() {
    	
    }
	public String getCodeArticles() {
		return codeArticles;
	}

	public void setCodeArticles(String codeArticles) {
		this.codeArticles = codeArticles;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public BigDecimal getPrixUnitaireHT() {
		return prixUnitaireHT;
	}

	public void setPrixUnitaireHT(BigDecimal prixUnitaireHT) {
		this.prixUnitaireHT = prixUnitaireHT;
	}

	public BigDecimal getTauxTva() {
		return tauxTva;
	}

	public void setTauxTva(BigDecimal tauxTva) {
		this.tauxTva = tauxTva;
	}

	public BigDecimal getPrixUnitaireTTc() {
		return prixUnitaireTTc;
	}

	public void setPrixUnitaireTTc(BigDecimal prixUnitaireTTc) {
		this.prixUnitaireTTc = prixUnitaireTTc;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public Categorie getCategorie() {
		return categorie;
	}

	public void setCategorie(Categorie categorie) {
		this.categorie = categorie;
	}

	public long getIdArticle() {
		return idArticle;
	}

	public void setIdArticle(long idArticle) {
		this.idArticle = idArticle;
	}
	
	
	
}
